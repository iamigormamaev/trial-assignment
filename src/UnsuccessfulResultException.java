public class UnsuccessfulResultException extends Exception {
    public UnsuccessfulResultException() {
    }

    public UnsuccessfulResultException(String message) {
        super(message);
    }

    public UnsuccessfulResultException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsuccessfulResultException(Throwable cause) {
        super(cause);
    }

    public UnsuccessfulResultException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
