import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Implementation {

    public Properties readConfig(String configName) throws IOException {
        Properties result = new Properties();
        BufferedReader br = new BufferedReader(new FileReader(configName));
        result.load(br);
        br.close();
        return result;
    }

    public List<String> getStringsToMatchFromConfig(Properties prop) {
        List<String> result = new ArrayList<>();
        for (Map.Entry<Object, Object> pair :
                prop.entrySet()) {
            if (!pair.getKey().equals("path") && !pair.getKey().equals("id")) {
                String s = (String) pair.getValue();
                result.add(s.substring(1, s.length() - 1));
            }
        }
        return result;
    }

    public void matchStringsWithFile(String filePath, String sectionId, List<String> stringsToMatch) throws IOException, UnsuccessfulResultException {
        Map<String, List<String>> sections = parseFileToIdSectionsMap(System.getProperty("user.dir") + filePath);
        if (sections.get(sectionId) != null) {
            matchStrings(sections.get(sectionId), stringsToMatch);
        } else {
            throw new UnsuccessfulResultException("Unable to find section with id: \"" + sectionId + "\"");
        }
    }

    private Map<String, List<String>> parseFileToIdSectionsMap(String filePath) throws IOException {
        Map<String, List<String>> result = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        String currentString;
        while ((currentString = br.readLine()) != null) {
            if (currentString.matches("\\[id\\]")) {
                String id = br.readLine().replace("id= ", "");
                List<String> sectionStrings = new ArrayList<>();
                while (!(currentString = br.readLine()).isEmpty()) {
                    sectionStrings.add(currentString);
                }
                result.put(id, sectionStrings);
            }
        }
        br.close();
        return result;
    }

    private void matchStrings(List<String> strings, List<String> patterns) throws UnsuccessfulResultException {
        for (String pattern :
                patterns) {
            boolean match = false;
            for (String s :
                    strings) {
                if (s.matches(pattern)) {
                    match = true;
                    break;
                }
            }
            if (!match) throw new UnsuccessfulResultException("Unable to find string: \"" + pattern + "\"");
        }
    }
}
