import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Implementation impl = new Implementation();
        Properties config;
        try {
            config = impl.readConfig(System.getProperty("user.dir") + args[0]);
        } catch (ArrayIndexOutOfBoundsException | IOException e) {
            System.out.println("Unable to read config file:\"" + System.getProperty("user.dir") + args[0] + "\". Enter the correct path to the config file in the first program argument.");
            return;
        }

        List<String> stringsToMatch = impl.getStringsToMatchFromConfig(config);
        try {

            impl.matchStringsWithFile(config.getProperty("path"), config.getProperty("id"), stringsToMatch);
        } catch (IOException e) {
            System.out.println("Unable to find file:\"" + config.getProperty("path") + "\". Please check argument \"path\".");
            return;
        } catch (UnsuccessfulResultException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("Success!");
    }
}
